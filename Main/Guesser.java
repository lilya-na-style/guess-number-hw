package Main;

import java.util.Scanner;

public class Guesser {
    private static int number = 0;
    private static int guessNumber;

    public static boolean getRightNumber(int number, int guessNumber, int attempt) {
        if (guessNumber == number) {
            return true;
        }
        System.out.println("Поздравляю! Ты угадал задуманное число за " + attempt + " попыток!");
        return false;
    }


    public static boolean runGuesser() {
        int attempt = 1;
        int count = 5;
        String out = "";

        Scanner userReader = new Scanner(System.in);

        do {
            System.out.print("Введи любое число:\n");

            if (userReader.hasNextInt()) {

                guessNumber = userReader.nextInt();

                if (guessNumber > 100 || guessNumber < 0) {

                    System.out.println("Ты можешь ввести число только не меньше 0 и не больше 100!");

                    break;
                }
                if (guessNumber == number) {
                    getRightNumber(number, guessNumber, attempt);


                    break;


                } else if (guessNumber < number && guessNumber >= 0) {
                    System.out.println("Твое число меньше. Осталось " + (count - attempt) + " попытки");

                } else if (guessNumber > number && guessNumber <= 100) {

                    System.out.println("Твое число больше. Осталось " + (count - attempt) + " попытки");
                }
                if (attempt == 5) {

                    System.out.println("К сожалению, ты исчерпал все попытки. Попробуй ещё");

                    break;
                }
                attempt++;

            } else {

                if (userReader.hasNextLine()) {
                    
                    out = userReader.next();

                    if (out.equals("exit")) {

                        System.out.println("Вы вышли из приложения. До встречи!");
                        break;


                    } else
                        System.out.println("Приложение работает только с целыми числами!");
                }

            }

        } while (guessNumber != number);

        return false;
    }



}


