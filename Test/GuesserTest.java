package Test;
import Main.Guesser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;


public class GuesserTest {

    private final Guesser cut = new Guesser();

    static Arguments [] getRightNumberTestArgs(){
        return new Arguments[]{
                Arguments.arguments(true, 98, 98, 2),
                Arguments.arguments(false, 50, 67, 3),
                Arguments.arguments(true, 67, 67, 4),
                Arguments.arguments(false, -2, 67, 4),
                Arguments.arguments(false, 105, 67,1)
        };
    }

    @ParameterizedTest
    @MethodSource("getRightNumberTestArgs")

    void getRightNumber(boolean result, int number, int guessNumber, int attempts){
        result = cut.getRightNumber(number, guessNumber, attempts);
        Assertions.assertTrue(number == guessNumber);
       // Assertions.assertFalse(number != guessNumber);


    }
}


